
//2.
db.Fruits.aggregate([
       {$match: {onSale:true}},{$count:"fruits"}
     
]);


// {
//     "fruits" : 3
// }


//3.

db.Fruits.aggregate([
       {$match: {stock: {$gt:20}}},
       {$count:"fruits"}
     
]);

// {
//     "fruits" : 1
// }


//4.
db.Fruits.aggregate([
        {$match: {onSale:true} } ,
       {$group:
           {_id: "$supplier_id",
           avgPrice: {$avg : "$price"}
          
         }
     }

   
]);


/*{
    "_id" : 1.0,
    "avgPrice" : 45.0
}

{
    "_id" : 2.0,
    "avgPrice" : 20.0
}
*/



//5.

db.Fruits.aggregate([
      
       {$group:
           {_id: "$supplier_id",
           maxPrice: {$max : "$price"}
          
         }
     }

   
]);

/*{
    "_id" : 1.0,
    "maxPrice" : 50.0
}*/

/*{
    "_id" : 2.0,
    "maxPrice" : 120.0
}*/




//6.


db.Fruits.aggregate([
      
       {$group:
           {_id: "$supplier_id",
           minPrice: {$min : "$price"}
          
         }
     }

   
]);

/*{
    "_id" : 1.0,
    "minPrice" : 40.0
}

{
    "_id" : 2.0,
    "minPrice" : 20.0
}*/